# Search Protocol - Group 28

Search string

~~~~~ {.searchstring }
("Blockage*" OR "obstacle*") AND ("5G networks*") 
~~~~~

Number of papers: 53

Inclusion criteria:

1.It includes fast and reliable network along with high frequencies.
2.It includes blockages in 5g network.
3.Any papers from 2015 to 2021


Exclusion criteria:

1.congestion window size (cwnd), throughput.

